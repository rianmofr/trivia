from . import auth_bp
from app import login_manager
from entrypoint import app
from flask import render_template, redirect, url_for, flash, request, session, g, current_app
from flask_principal import AnonymousIdentity, identity_changed, Identity
from .models import Usuario
from .forms.login import LoginForm
from .forms.register import RegisterForm
from datetime import datetime
from flask_login import current_user, login_user, login_required, logout_user

# controles de acceso a trivia con login, register y la herramienta
@auth_bp.route('/trivia/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('public.index'))
    form = LoginForm()
    if form.validate_on_submit():
        #get by email valida
        user = Usuario.get_by_email(form.email.data)
        if user is not None and user.check_password(form.password.data):
            # funcion provista por Flask-Login, el segundo parametro gestion el "recordar"
 #           login_user(user, remember=form.remember_me.data)
            login_user(user,remember=True)
            identity_changed.send(app, identity=Identity(user.id))
            next_page = request.args.get('next', None)
            if not next_page:
                next_page = url_for('public.index')
            return redirect(next_page)

        else:
            flash('Usuario o contraseña inválido')
            return redirect(url_for('auth.login'))
    # no loggeado, dibujamos el login con el forms vacio
    return render_template('login.html', form=form)


#le decimos a Flask-Login como obtener un usuario
@login_manager.user_loader
def load_user(user_id):
    return Usuario.get_by_id(int(user_id))


@auth_bp.route("/trivia/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('public.index'))
    form = RegisterForm()
    error = None
    if form.validate_on_submit():
        username = form.username.data
        email = form.email.data
        password = form.password.data
        # Comprobamos que no hay ya un usuario con ese email
        user = Usuario.get_by_email(email)
        if user is not None:
            flash('El email {} ya está siendo utilizado por otro usuario'.format(email))
        else:
            # Creamos el usuario y lo guardamos
            user = Usuario(name=username, email=email, admin=False)
            user.set_password(password)
            user.register_date = datetime.today();
            user.save()
            # Dejamos al usuario logueado
            login_user(user, remember=True)
            return redirect(url_for('public.index'))
    return render_template("register.html", form=form)

@auth_bp.route("/trivia/logout")
@login_required
def logout():
    logout_user()
    # Flask-Principal: Remove session keys
    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)

    # Flask-Principal: the user is now anonymous
    identity_changed.send(app, identity=AnonymousIdentity())
    return redirect(url_for('public.index'))

@auth_bp.route('/trivia/clasificacion')
@login_required
def clasificacion():
    usuarios = Usuario.query.filter(Usuario.score.isnot(None)).order_by(Usuario.score)
    return render_template('clasificaciones.html', usuarios=usuarios)


def actualizarScoreUsuario(duracion):
    usuario = current_user
    if usuario.score is None:
        usuario.score = duracion.total_seconds()
    else:
        if usuario.score > duracion.total_seconds():
            usuario.score = duracion.total_seconds()
        else:
            return
    usuario.save()


# solucion porque g queda none
@auth_bp.before_request
def before_request():
    if hasattr(current_user, 'id'):
        g.identity = Identity(current_user.id)
        app_actual = current_app._get_current_object()
        identity_changed.send(app_actual, identity=Identity(current_user.id))
    else:
        g.identity = AnonymousIdentity