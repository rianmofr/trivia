from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import InputRequired, Length, Email, AnyOf

# Formulario para el login
class LoginForm(FlaskForm):
    email = StringField('Email', validators=[InputRequired(message='ingresa tu email')])
    password = PasswordField('Password', validators=[InputRequired(message='ingresa una clave')])
    remember_me = BooleanField('Recuérdame')
    submit = SubmitField('Login')