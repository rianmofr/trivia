from . import restricted_bp
import flask_principal
from app import db, admin
from flask_login import current_user, login_required
from flask_principal import Permission, RoleNeed, UserNeed, identity_loaded, Identity
from flask_admin.contrib.sqla import ModelView
from flask import render_template, redirect, url_for, request
from flask_admin import AdminIndexView
from flask_login import current_user
from app.public.models import Categoria, Pregunta, Respuesta
from app.auth.models import Usuario, Role

# Flask-Principal: Creamos un permiso con una sola necesidad que debe ser satisfecho para entrar al admin.
admin_permission = Permission(RoleNeed('admin'))
# Flask-Principal: Agregamos las necesidades a una identidad, una vez que se loguee el usuario.
@identity_loaded.connect
def on_identity_loaded(sender, identity):
    # Seteamos la identidad al usuario
    identity.user = current_user
    # Agregamos una UserNeed a la identidad, con el id del usuario actual.
    if hasattr(current_user, 'id'):
            identity.provides.add(UserNeed(current_user.id))
    # Agregamos a la identidad la lista de roles que posee el usuario actual.
    if hasattr(current_user, 'roles'):
        for role in current_user.roles:
            identity.provides.add(RoleNeed(role.rolename))
"""
@principal.identity_saver
def save_identity_to_weird_usecase(identity):
    g.identity = identity
"""

class MyModelView(ModelView):
    def is_accessible(self):
        has_auth = current_user.is_authenticated
        identidad = generar_identidad()
        if isinstance(identidad, type(flask_principal.AnonymousIdentity)):
            return False
        has_perm = admin_permission.allows(identidad)
        return has_auth and has_perm

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))

class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        has_auth = current_user.is_authenticated
        identidad = generar_identidad()
        if isinstance(identidad, type(flask_principal.AnonymousIdentity)):
            return False
        has_perm = admin_permission.allows(identidad)
        return has_auth and has_perm

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))
        #return redirect(url_for('error',error=403))
        #return render_template('error.html', error=403)

admin._set_admin_index_view(MyAdminIndexView())
# Los modelos que queremos mostrar en el admin

admin.add_view(MyModelView(Pregunta, db.session))
admin.add_view(MyModelView(Respuesta, db.session))
admin.add_view(MyModelView(Usuario, db.session))
admin.add_view(MyModelView(Role, db.session))
admin.add_view(MyModelView(Categoria, db.session))

def generar_identidad():
    if hasattr(current_user, 'id'):
        identity = Identity(current_user.id)
        identity.provides.add(UserNeed(current_user.id))
        if hasattr(current_user, 'roles'):
            for role in current_user.roles:
                identity.provides.add(RoleNeed(role.rolename))
        return identity
    else:
        return flask_principal.AnonymousIdentity

@restricted_bp.route('/test')
@login_required
@admin_permission.require(http_exception=403)
def test_principal():
    return render_template('test.html')


