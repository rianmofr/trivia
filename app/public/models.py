from app import db

class Categoria(db.Model):
   # __tablename__ = 'categoria'
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(64), nullable=False, index=True, unique=True)
    preguntas = db.relationship('Pregunta', backref='categoria', lazy='dynamic')

    def __repr__(self):
        return f'<Categoria: {self.descripcion}>'


class Pregunta(db.Model):
    #__tablename__ = 'pregunta'
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(255), nullable=False, unique=True)

    categoria_id = db.Column(db.Integer, db.ForeignKey('categoria.id'))
    respuestas = db.relationship('Respuesta', backref='pregunta', lazy='dynamic')

    def __repr__(self):
        return f'<Pregunta {self.descripcion}>'

class Respuesta(db.Model):
   # __tablename__ = 'respuesta'
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(300), nullable=False, unique=True)
    correcta = db.Column(db.Boolean, nullable=False)
    pregunta_id = db.Column(db.Integer, db.ForeignKey('pregunta.id'))

    def __repr__(self):
        return f'<Respuesta {self.descripcion}>'

