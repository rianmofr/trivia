from . import public_bp
from app import login_required
from flask import render_template, session
import random
from datetime import datetime
from app.auth.routes import actualizarScoreUsuario

# importamos los modelos a usar
from .models import Categoria, Pregunta, Respuesta

@public_bp.route('/trivia')
def index():
    inicializo_trivia()
    return render_template('index.html', fin=False, tiempo=(tiempoDeJuegoDisplay(tiempoDeJuego())))

@public_bp.route('/trivia/categorias/<int:comienzojuego>', methods=['GET'])
@login_required
def mostrarcategorias(comienzojuego):
    if comienzojuego == 0:
        inicializo_trivia()
    categorias = Categoria.query.all()
    dicc = session["categoriasFinalizaPendiente"]
    return render_template('categorias.html', categorias=categorias, dicc=dicc,tiempo=tiempoDeJuegoDisplay(tiempoDeJuego()))


@public_bp.route('/trivia/<int:id_categoria>/pregunta', methods=['GET'])
@login_required
def mostrarpregunta(id_categoria):
    categoria = Categoria.query.get(id_categoria)
    preguntas = Pregunta.query.filter_by(categoria_id=id_categoria).all()
    # elegir pregunta aleatoria pero de la categoria adecuada
    pregunta = random.choice(preguntas)
    #respuestas = Respuesta.query.filter_by(pregunta_id=pregunta.id).all()
    categ = Categoria.query.get(id_categoria)
    return render_template('preguntas.html', categoria=categ, pregunta=pregunta, respuestas=pregunta.respuestas)

@public_bp.route('/trivia/<id_pregunta>/resultado/<id_respuesta>', methods=['GET'])
@login_required
def evaluarRespuestaElegida(id_pregunta,id_respuesta):
    respuesta = Respuesta.query.get(id_respuesta)
    #respuestas = Respuesta.query.filter_by(pregunta_id=id_pregunta).all()
    pregunta = Pregunta.query.get(respuesta.pregunta_id)
    for r in pregunta.respuestas:
        if r.id == int(id_respuesta):
            if respuesta.correcta:
                categoria = Categoria.query.get(pregunta.categoria_id)
                dicc = session["categoriasFinalizaPendiente"]
                dicc[categoria.descripcion]="Finalizada"
                session["categoriasFinalizaPendiente"] = dicc
                if terminoTrivia():
                    duracion = tiempoDeJuego()
                    actualizarScoreUsuario(duracion)
                    inicializo_trivia()
                    return render_template('index.html',fin=True, tiempo=tiempoDeJuegoDisplay(duracion))
                else:
                    return render_template('resultado.html',correcta=True, categoria_id=pregunta.categoria_id)
            else:
                return render_template('resultado.html',correcta=False, categoria_id=pregunta.categoria_id)
    return  render_template('resultado.html',correcta=False)


def inicializo_trivia():
    session.clear()
    dicc={}
    categorias = Categoria.query.all()
    for categoria in categorias:
        dicc[categoria.descripcion]='Pendiente'
    session["inicio"] = datetime.today()
    session["categoriasFinalizaPendiente"]=dicc

def terminoTrivia():
    dicc = session["categoriasFinalizaPendiente"]
    for nombre in dicc:
        if dicc[nombre] == "Pendiente" :
            return False
    return True

def tiempoDeJuego():
    return datetime.today() - session["inicio"]

def tiempoDeJuegoDisplay(tiempo):
    return str(tiempo).split(".")[0]

