from . import errors_bp
from flask import render_template
from werkzeug.exceptions import HTTPException

@errors_bp.errorhandler(401)
def unathorized(e):
    return render_template('error.html', error=e.code)

@errors_bp.errorhandler(HTTPException)
def handle_exception(e):
    return render_template('error.html', error=e.code)